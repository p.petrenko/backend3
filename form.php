<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>WEB-Project</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
  </head>
  <body>
    <header id="header_site">
      <div id="logo_and_name">
        <img id="logo" src="https://www.flaticon.com/svg/static/icons/svg/203/203678.svg" alt="Logo" />
        <h1>IndexPro</h1>
      </div>
    </header>
    <div class="container py-5 my-3 px-4" id="main">
      <div class="row d-flex">
        <div class="col-12 order-3 my-3 order-md-2">
          <table class="my-0 mx-auto" id="special">
            <tr>
              <th class="py-1 py-md-2 px-md-4" colspan="4">Курсы обучения веб-разработке</th>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">Время/Дни занятий</td>
              <td class="py-1 py-md-2 px-md-4">Понедельник</td>
              <td class="py-1 py-md-2 px-md-4">Среда</td>
              <td class="py-1 py-md-2 px-md-4">Пятница</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">13:00</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">14:00</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">15:00</td>
              <td class="py-1 py-md-2 px-md-4" colspan="3">HTML/CSS</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">16:00</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">17:00</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
            </tr>
          </table>
        </div>
        <div class="col-12 my-3 order-2 order-md-3" id="whole_form">
          <h6>Форма</h6>
          <form action="."
            method="POST">
            <label>
            Имя:<br>
            <input name="name"
              placeholder="Введите имя"
              value="Иванов Иван Иванович"/>
            </label><br><br>
            <label>
            E-mail:<br>
            <input name="email"
              placeholder="Введите email"
              value="web@project.com"
              type="email" />
            </label><br><br>
            <label>
            Дата рождения:<br />
            <input name="birthday" id="birth_date"
              value="1931-04-19"
              type="date" />
            </label><br><br>
            Пол:<br>
            <label><input type="radio" checked="checked" name="gender" value="M" />
            Мужской
            </label>
            <label><input type="radio" name="gender" value="F" />
            Женский
            </label>
            <label><input type="radio" name="gender" value="NB" />
            Небинарная персона
            </label><br><br>
            Количество конечностей:<br>
            <label><input type="radio" name="limb_number" value="0" />
            0
            </label>
            <label><input type="radio" name="limb_number" value="1" />
            1
            </label>
            <label><input type="radio" name="limb_number" value="2" />
            2
            </label>
            <label><input type="radio" name="limb_number" value="3" />
            3
            </label>
            <label><input type="radio" name="limb_number" value="4" />
            4
            </label>
            <label><input type="radio" checked="checked" name="limb_number" value="5" />
            Другое
            </label>
            <br><br>
            <label>
              Сверхспособности:
              <br>
              <select name="superpowers[]" multiple="multiple">
                <option value="1">Бессмертие</option>
                <option value="2">Прохождение сквозь стены</option>
                <option value="3">Левитация</option>
              </select>
            </label>
            <br><br>
            <label>
            Биография:<br>
            <textarea class="message" id="biography" placeholder="Расскажите о себе" rows="7" cols="50" name="biography">Давным-давно, в далёкой-далёкой галактике...</textarea>
            </label><br><br>
            <br>
            <label for="cb" class="c_box">
            <input type="checkbox" id="cb" value="" name="contract"><span></span>
            С контрактом ознакомлен
            </label><br>
            <a id="submit_button"></a>
            <input class="py-2 px-3 mt-0 mb-4" type="submit" value="Отправить"><br>
          </form>
        </div>
      </div>
    </div>
    <a id="before_footer"></a>
    <footer id="footer_site">
      <b>@In code we trust</b>
    </footer>
  </body>
</html>
