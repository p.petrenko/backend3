<?php
header('Content-Type: text/html; charset=UTF-8');

//Only report fatal errors and parse errors.
error_reporting(E_ERROR | E_PARSE);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  include('form.php');
  if (!empty($_GET['save'])) {
    print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>
                Спасибо, результаты сохранены.
              </div>
            </div>";
  }
    exit();
}

$errors = FALSE;

$errors_message = "Отправка данных прервана из-за следующих ошибок: <br/>";

$trimmedPost = [];

foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

if (empty($trimmedPost['name'])) {
  //print('Заполните имя.<br/>');
  $errors_message.= 'Вы не заполнили имя.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  //print('Заполните email.<br/>');
  $errors_message.= 'Вы не заполнили email или ввели неверные данные.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  //print('Заполните дату рождения.<br/>');
  $errors_message.= 'Вы не заполнили дату рождения или ввели неверные данные.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[MF]$|^(NB)$/', $trimmedPost['gender'])) {
  //print('Заполните пол.<br/>');
  $errors_message.= 'Вы неверно заполнили пол.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-5]$/', $trimmedPost['limb_number'])) {
  //print('Заполните количество конечностей.<br/>');
  $errors_message.= 'Вы ввели недопустимое количество конечностей.<br/>';
  $errors = TRUE;
}
$superpowers_error = FALSE;
foreach ($trimmedPost['superpowers'] as $v)
  if (!preg_match('/[1-3]/', $v)) {
    //print('Неверные суперспособности.<br/>');
    $superpowers_error = TRUE;
    $errors = TRUE;
  }
if($superpowers_error)
  $errors_message.= 'Вы ввели неверные суперспособности.<br/>';
if (!isset($trimmedPost['contract'])) {
  //print('Вы не ознакомились с контрактом.<br/>');
  $errors_message.= 'Вы не ознакомились с контрактом.<br/>';
  $errors = TRUE;
}

/*foreach ($trimmedPost as $key => $value)
  echo $key,"=>",$value, '<br />';

foreach ($trimmedPost['superpowers'] as $v)
  echo $v,'<br />';*/

if ($errors) {
  include('form.php');
  print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>";
  echo "{$errors_message}";  
  print "</div>
            </div>";           
  exit();
}

$user = 'u20323';
$pass = '6300522';
$db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO Form SET name = ?, email = ?, birthday = ?, 
    gender = ? , limb_number = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limb_number'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO Form_Abilities SET form_id = ?, ability_id = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt2 -> execute([$id, $s]);
  $db->commit();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  exit();
}


header('Location: ?save=1');
